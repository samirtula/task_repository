<?php
use Bitrix\Main\Config\Option;
use Intensa\Parser\Main;

function getNews()
{
    $rssName = Option::get('intensa.parser', 'RSS_CHANNEL');
    $iblockId = Option::get('intensa.parser', 'IBLOCK_ID');
    if (!empty($iblockId) && !empty($rssName)) {
    $newRss = new Main($rssName, $iblockId);
    } else {
        return false;
    }
}

?>