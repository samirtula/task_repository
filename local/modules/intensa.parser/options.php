<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Intensa\Parser\Main;

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);
Loader::includeModule($module_id);
CModule::IncludeModule("iblock");

if (CModule::IncludeModule("iblock")) {
    $blockTypesList = Bitrix\Iblock\TypeTable::getList([
        'select' => ['*', 'LANG_MESSAGE'], 'filter' => ['=LANG_MESSAGE.LANGUAGE_ID' => 'ru']])->FetchAll();
    $typesArr = [];
    $iblocks = \Bitrix\Iblock\IblockTable::getList([
        'select' => ['ID', 'NAME'],
    ])->FetchAll();
}
?>
<?php if (!($request->isPost())): ?>
    <form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>"
          method="post">
        <?= bitrix_sessid_post() ?>
        <select name="iblock_selected" id="block">
            <?php foreach ($iblocks as $iblock): ?>
                <option value="<?php echo $iblock['ID'] ?>"><?php echo $iblock['NAME'] ?></option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" name="lang" value="<? echo(LANG); ?>"/>
        <input name='rss_address' style="width: 200px" class="input_rss" type="text"
               placeholder="<? echo(Loc::getMessage("PARSER_RSS_CHANNEL")); ?>">
        <br>
        <p style="margin-top: 30px; font-weight: bold">Текущий rss
            канал: <? echo Option::get("$module_id", 'RSS_CHANNEL');
            echo(' ID = ' . Option::get("$module_id", 'IBLOCK_ID')); ?>
        </p>
        <input style="margin-top: 30px" type="submit" value="Сохранить">
    </form>
<?php else:
    Option::set($module_id, 'RSS_CHANNEL', $request["rss_address"]);
    Option::set($module_id, 'IBLOCK_ID', $request["iblock_selected"]);
    $newRss = new Main(Option::get($module_id, 'RSS_CHANNEL'), Option::get($module_id, 'IBLOCK_ID'));
    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . $module_id . "&lang=" . LANG); ?>
<?php endif; ?>