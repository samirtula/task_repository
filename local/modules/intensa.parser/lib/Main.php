<?php

namespace Intensa\Parser;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;


class Main
{
    public function __construct($rss, $iblockID)
    {
        $xml = file_get_contents("$rss");
        $parser = simplexml_load_string($xml);
        $articlesArr = [];
        $itemsObj = $parser->channel->item;
        for ($i = 0; $i < count($itemsObj); $i++) {
            $articlesArr[$i]['name'] = (string)$itemsObj[$i]->title;
            $articlesArr[$i]['link'] = ['link' => (string)$itemsObj[$i]->link];
            $articlesArr[$i]['date'] = (string)date("d.m.Y H:i", strtotime($itemsObj[$i]->pubDate));
            $articlesArr[$i]['description'] = (string)$itemsObj[$i]->description;
            $articlesArr[$i]['imgSrc'] = '';
            $descriptionIncludeSrc = explode('<img src=', $articlesArr[$i]['description']);
            foreach ($descriptionIncludeSrc as $stringIncludeSrc) {
                $imageSrc = explode('"', $stringIncludeSrc);
                foreach ($imageSrc as $src) {
                    if (strpos($src, 'jpg') || strpos($src, 'jpeg') || strpos($src, 'png')) {
                        $articlesArr[$i]['imgSrc'] = $src;
                    }
                }
            }
        }
        foreach ($articlesArr as $value) {
            $this->addElement($iblockID, $value['name'], $value['link'], $value['date'], $value['description'], $value['imgSrc']);
        }
    }
    public function addElement($iblockId, $name, $link, $date, $description, $imgSrc)
    {
        \CModule::IncludeModule("iblock");
        $element = new \CIBlockElement();
        $element->Add([
            "IBLOCK_ID" => $iblockId,
            "NAME" => $name,
            "DATE_CREATE" => $date,
            "DETAIL_TEXT" => $description,
            "DETAIL_TEXT_TYPE" => 'html',
            "PROPERTY_VALUES" => ['DATE_C' => $date, 'LINK' => $link],
            "CODE" => $this->Transliterate($name),
            "DETAIL_PICTURE" =>  \CFile::MakeFileArray($imgSrc)
        ]);
        echo "Error: " . $element->LAST_ERROR;
    }
    function Transliterate($string)
    {
        $cyr = [
            "Щ", "Ш", "Ч", "Ц", "Ю", "Я", "Ж", "А", "Б", "В", "Г", "Д", "Е", "Ё", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ь", "Ы", "Ъ", "Э", "Є", "Ї",
            "щ", "ш", "ч", "ц", "ю", "я", "ж", "а", "б", "в", "г", "д", "е", "ё", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ь", "ы", "ъ", "э", "є", "ї"
        ];
        $lat = [
            "Shh", "Sh", "Ch", "C", "Ju", "Ja", "Zh", "A", "B", "V", "G", "D", "Je", "Jo", "Z", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "Kh", "'", "Y", "`", "E", "Je", "Ji",
            "shh", "sh", "ch", "c", "ju", "ja", "zh", "a", "b", "v", "g", "d", "je", "jo", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "'", "y", "`", "e", "je", "ji"
        ];
        for ($i = 0; $i < count($cyr); $i++) {
            $c_cyr = $cyr[$i];
            $c_lat = $lat[$i];
            $string = str_replace($c_cyr, $c_lat, $string);
        }
        $string = preg_replace("/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]e/", "\${1}e", $string);
        $string = preg_replace("/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]/", "\${1}'", $string);
        $string = preg_replace("/([eyuioaEYUIOA]+)[Kk]h/", "\${1}h", $string);
        $string = preg_replace("/^kh/", "h", $string);
        $string = preg_replace("/^Kh/", "H", $string);
        return $string;
    }
}