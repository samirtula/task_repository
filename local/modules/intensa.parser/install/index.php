<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;

Loc::loadMessages(__FILE__);

class intensa_parser extends CModule
{
    public function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {
            $arModuleVersion = [];
            include_once(__DIR__ . '/version.php');
            if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
                $this->MODULE_VERSION = $arModuleVersion["VERSION"];
                $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            }
            $this->MODULE_NAME = Loc::getMessage("PARSER_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("PARSER_DESCRIPTION");
            $this->MODULE_DESCRIPTION = GetMessage("PARSER_INSTALL_DESCRIPTION");
            $this->MODULE_ID = str_replace("_", ".", get_class($this));
        }
    }

    public function DoInstall()
    {
        global $APPLICATION;
        if (CheckVersion(ModuleManager::getVersion('main'), "14.00.00")) {
            $this->InstallFiles();
            ModuleManager::registerModule($this->MODULE_ID);
            $this->InstallAgents();
        } else {
            $APPLICATION->ThrowException(
                Loc::getMessage("PARSER_INSTALL_ERROR")
            );
        }
        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("PARSER_INSTALL_TITLE") . " \"" . Loc::getMessage("PARSER_NAME") . "\"",
            __DIR__ . "/step.php"
        );
    }

    public function InstallAgents()
    {
        $now = date("d.m.Y H:i:s");
        CAgent::AddAgent(
            "getNews()",
            $this->MODULE_ID,
            "N",
            60,
           '',
            "Y",
            '',
            50);
    }

    public function UnInstallAgents()
    {
        CAgent::RemoveAgent('getNews()', $this->MODULE_ID);
    }

    public function InstallFiles()
    {
        CopyDirFiles(
            __DIR__ . "/assets/scripts",
            Application::getDocumentRoot() . "/bitrix/js/" . $this->MODULE_ID . "/", true, true
        );
        CopyDirFiles(
            __DIR__ . "/assets/styles",
            Application::getDocumentRoot() . "/bitrix/css/" . $this->MODULE_ID . "/", true, true
        );
    }

    public function InstallDB()
    {
        return false;
    }

    public function DoUninstall()
    {
        global $APPLICATION;
        $this->UnInstallFiles();
        $this->UnInstallDB();
        $this->UnInstallAgents();
        ModuleManager::unRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("PARSER_UNINSTALL_TITLE") . " \"" . Loc::getMessage("PARSER_NAME") . "\"",
            __DIR__ . "/unstep.php"
        );
    }

    public function UnInstallFiles()
    {
        Directory::deleteDirectory(Application::getDocumentRoot() . "/bitrix/js/" . $this->MODULE_ID);
        Directory::deleteDirectory(Application::getDocumentRoot() . "/bitrix/css/" . $this->MODULE_ID);
    }

    public function UnInstallDB()
    {
        Option::delete($this->MODULE_ID);
    }
}